﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using SharedObjects;

namespace Controller
{
    public static class PlayerStatus
    {
        public static PublicEndPoint myEndpoint { get; set; }
        private static Boolean loggedIn = false;
        private static Boolean logInSent = false;
        private static Boolean haveGameList = false;
        private static Boolean gameListSent = false;
        private static Boolean joinGameSent = false;
        private static Boolean joinedGame = false;
        private static DateTime aliveRequest = new DateTime();

        public static Boolean LoggedIn { get { return loggedIn; } set { loggedIn = value; OnChanged(EventArgs.Empty); } }
        public static Boolean LogInSent { get { return logInSent; } set { logInSent = value; OnChanged(EventArgs.Empty); } }
        public static Boolean HaveGameList { get { return haveGameList; } set { haveGameList = value; OnChanged(EventArgs.Empty); } }
        public static Boolean GameListSent { get { return gameListSent; } set { gameListSent = value; OnChanged(EventArgs.Empty); } }
        public static Boolean JoinGameSent { get { return joinGameSent; } set { joinGameSent = value; OnChanged(EventArgs.Empty); } }
        public static Boolean JoinedGame { get { return joinedGame; } set { joinedGame = value; OnChanged(EventArgs.Empty); } }
        public static DateTime AliveRequest { get { return aliveRequest; } set { aliveRequest = value; OnChanged(EventArgs.Empty); } }

        public static GameInfo[] gameInfo = null;
        public static Penny[] pennies = null;
        public static int lifePoints = 0;
        public static int gameId = 0;

        public delegate void ChangedEventHandler(EventArgs e);

        public static event ChangedEventHandler Changed;

        // Invoke the Changed event; called whenever list changes
        public static void OnChanged(EventArgs e)
        {
            if (Changed != null)
                Changed(e);
        }
    }
}
