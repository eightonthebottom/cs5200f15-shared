﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Concurrent;
using Messages;
using Players;
using SharedObjects;
using System.Threading;
using log4net;

namespace Connection_UDP_JSON
{
    public class Sender
    {
        private UdpClient myUdpClient;
        private Boolean exit;
        private ConcurrentQueue<Envelope> sendQueue;
        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public Sender(UdpClient client)
        {
            myUdpClient = client;
            sendQueue = new ConcurrentQueue<Envelope>();
            exit = false;
        }

        /* Send the message out UDP client
         * @args: message-to be sent, peer-address to send too
         * 
         */
        private void sendMessage(Message message, IPEndPoint remoteEP)
        {
            Boolean success = false;
            int count = 0;

            /* don't try and send if the endpoint failed */
            if (remoteEP.Equals(null))
                count = 100;

            /* try 10 times to send message */
            while (!success && count < 10) //this only fail if this side fails to send
            {
                byte[] bytes = message.Encode();
                int result = myUdpClient.Send(bytes, bytes.Length, remoteEP);
                if (result != 0)
                    success = true;

                log.Debug("Message Sent: ");
                log.Debug(System.Text.Encoding.UTF8.GetString(bytes));
            }
        }
        //-------------------------------------------------------------------------------

        /* Adds a message to the queue to be sent out by main loop */
        public void addEnvelopeToSendQueue(Envelope envelope)
        {
            sendQueue.Enqueue(envelope);
        }
        //-------------------------------------------------------------------------------

        /* Main Thread loop, keeps running (just waiting to send messages)
         * 
         */
        public void waitToSend()
        {
            exit = false;
            while (!exit)
            {
                /* wait until something to send */
                while (sendQueue.Count < 1)
                {
                    Thread.Sleep(50);
                }

                Envelope catcher = null;
                Boolean success = sendQueue.TryDequeue(out catcher);
                
                /* make sure message it valid, don't send nulls */
                if (catcher != null)
                {
                    if (catcher.message != null && catcher.endpoint != null && success)
                    {
                        sendMessage(catcher.message, catcher.endpoint);
                    }
                }
            }
        }
        //-------------------------------------------------------------------------------

        /* A way to kill the loop, thus killing thread
         * @args: exit
         * 
         */
        public void setExit(Boolean exit)
        {
            this.exit = exit;
        }
    }
}
