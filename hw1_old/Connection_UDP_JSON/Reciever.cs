﻿using System;
using System.Net;
using System.Net.Sockets;
using Messages;
using System.Threading;
using System.Collections.Concurrent;
using SharedObjects;
using Players;
using log4net;

namespace Connection_UDP_JSON
{
    public class Receiver
    {
        private UdpClient myUdpClient;
        private IPEndPoint localEP;
        private Boolean exit;
        private ConcurrentQueue<Envelope> receivedQueue;
        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public Receiver(UdpClient client, IPEndPoint ep)
        {
            myUdpClient = client;
            localEP = ep;
            receivedQueue = new ConcurrentQueue<Envelope>();
        }
        //-------------------------------------------------------------------------------

        public void receiveMessages()
        {
            exit = false;
            while (!exit)
            {
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
                Message message = null;

                /* try to catch an exception if remoteEP is no good */
                try
                {
                    /* this blocks for the timeout in ms or until message is reveived */
                    myUdpClient.Client.ReceiveTimeout = 5000;
                    byte[] bytes = myUdpClient.Receive(ref remoteEP);
                    message = Message.Decode(bytes);
                    log.Debug("Message Received: " + System.Text.Encoding.UTF8.GetString(bytes));
                }
                catch
                {
                    Thread.Sleep(1000);
                }
  
                if (message != null)
                {
                    log.Debug("Message added to receive queue");
                    Envelope envelop = new Envelope(message, message.ConversationId, remoteEP);
                    receivedQueue.Enqueue(envelop);                  
                    message = null;
                }
            }
        }
        //-------------------------------------------------------------------------------

        public void setExit(Boolean exit)
        {
            this.exit = exit;
        }
        //-------------------------------------------------------------------------------

        public Envelope getNextMessage()
        {
            if(receivedQueue.Count > 0)
            {
                Envelope result;
                receivedQueue.TryDequeue(out result);

                return result;
            }
            else
            {
                return null;
            }
        }
        //-------------------------------------------------------------------------------
    }
}
