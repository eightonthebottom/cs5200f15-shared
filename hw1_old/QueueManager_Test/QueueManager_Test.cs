﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using QueueManagerName;
using Messages;
using SharedObjects;

namespace QueueManager_Test
{
    [TestClass]
    public class QueueManager_Test
    {
        [TestMethod]
        public void Test_QueueManager()
        {
            QueueManager.Init();
            
            LogoutRequest message= new LogoutRequest();
            MessageNumber number = new MessageNumber();
            number.ProcessId = 10;
            message.ConversationId = number;
            message.MessageNr = number;

            Envelope e = new Envelope(message, number, null);
            QueueManager.addEnvelopeToQueue(e);

            Envelope back_e = QueueManager.getMessageByConversationId(number);
            Assert.IsNull(back_e);

            back_e = QueueManager.getMessageDefaultQueue();
            Assert.IsNotNull(back_e);
            Assert.AreSame(e, back_e);

            QueueManager.addNewQueueToDictionary(number);
            QueueManager.addEnvelopeToQueue(e);

            back_e = QueueManager.getMessageByConversationId(number);
            Assert.IsNotNull(back_e);
            Assert.AreSame(e, back_e);

            back_e = QueueManager.getMessageDefaultQueue();
            Assert.IsNull(back_e);

            QueueManager.addNewQueueToDictionary(null);
            QueueManager.getMessageByConversationId(null);
            QueueManager.addEnvelopeToQueue(null);
            QueueManager.addEnvelopeToQueue(new Envelope(null, null, null));
            QueueManager.addEnvelopeToQueue(new Envelope(new Message(), null, null));
            QueueManager.addEnvelopeToQueue(new Envelope(null, new MessageNumber(), null));
            Assert.IsTrue(true); //won't get here if exception is thrown 
        }
    }
}
