﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using Controller;
using Connection_UDP_JSON;
using Messages;
using SharedObjects;

namespace Connection_UDP_JSON_Test
{
    [TestClass]
    public class Connection_UDP_JSON_Test
    {
        [TestMethod]
        public void Communicator_Tests()
        {
            Communicator comm = new Communicator();
            Assert.IsNotNull(comm);
            Assert.IsNotNull(comm.getClient());
            Assert.IsNotNull(comm.getReceiver());
            Assert.IsNotNull(comm.getSender());
            Assert.IsNotNull(comm.getEndpoint());


            /* Test Send Receive Message */
            String builder = "localhost:" + comm.getEndpoint().Port;
            IPEndPoint temp = EndPointParser.Parse(builder);
            Reply m = new Reply();
            m.Success = true;
            m.Note = "here we go";
            MessageNumber num = new MessageNumber();
            MessageNumber.LocalProcessId = 10;
            num = MessageNumber.Create();
            m.ConversationId = num;
            m.MessageNr = num;
            Envelope e = new Envelope(m, new MessageNumber(), temp);

            comm.sendEnvelope(e);
            Thread.Sleep(1000);
            Envelope k = comm.getEnvelope();

            Assert.IsNotNull(k);

            Assert.AreNotSame(e, k);
            Reply a = k.message as Reply;
            Reply b = e.message as Reply;
            Assert.AreEqual(a.Success, b.Success);
            Assert.AreEqual(a.Note, b.Note);
            Assert.AreEqual(k.endpoint, temp);


            /* Test Send nulls */
            Envelope nllE = new Envelope(null, null, null);
            comm.sendEnvelope(nllE);
            nllE = new Envelope(null, null, temp);
            comm.sendEnvelope(nllE);
            nllE = new Envelope(null, new MessageNumber(), temp);
            comm.sendEnvelope(nllE);
            nllE = new Envelope(m, null, null);
            comm.sendEnvelope(nllE);
            comm.sendEnvelope(null);
            Assert.IsTrue(true); //if no exceptions
          
            /* Test Receive Nothing */
            Envelope nllRe = comm.getEnvelope();
            Assert.IsNull(nllRe);

            /* clean threads */
            comm.kill();

            Thread.Sleep(1000);
        }
    }
}
