﻿using System;
using System.Windows;
using System.Threading;
using System.Windows.Threading;
using System.Configuration;

using Players;
using Controller;
using SharedObjects;
using System.Windows.Controls;
using log4net;
using Conversations;
using QueueManagerName;
using Messages;
using Connection_UDP_JSON;

namespace DriverGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Player player;
        private int exitCount;
        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public MainWindow()
        {
            InitializeComponent();

            log4net.Config.XmlConfigurator.Configure();

            /* used for command line args */
            //String[] args = Environment.GetCommandLineArgs();
            //if (args == null)
            //{
            //    Application.Current.Shutdown();
            //}

            ///* get player from args */
            //player = new Player(args[1], args[2], args[3], args[4], args[5]);

            /* used for config file args */
            player = new Player(
                ConfigurationManager.AppSettings["Reg_Endpoint"],
                ConfigurationManager.AppSettings["First_Name"],
                ConfigurationManager.AppSettings["Last_Name"],
                ConfigurationManager.AppSettings["A_Number"], 
                ConfigurationManager.AppSettings["Alias"]
                );

            initUIcontent();
            ConversationManager m = new ConversationManager(player);

            PlayerStatus.Changed += delegate(EventArgs e)
            {
                Dispatcher.BeginInvoke(new Action(() => { updateAliveRequest(); }));
                Dispatcher.BeginInvoke(new Action(() => { updateRegLabel(); }));
                Dispatcher.BeginInvoke(new Action(() => { updateGameInfo(); }));
            };

            exitCount = 0;
        }

        private void initUIcontent()
        {
            label_PlayerName.Content = player.idenitity.Alias;
            label_reg.Content = "init...";
            label_lastAliveSent.Content = "";

            dataGrid_pennies.Visibility = System.Windows.Visibility.Hidden;
            label_lifepoints.Visibility = System.Windows.Visibility.Hidden;
            label_gameID.Visibility = System.Windows.Visibility.Hidden;
            label_penny.Visibility = System.Windows.Visibility.Hidden;
        }
        

        /* UI Updates --------------------------------------------------------------*/
        public void updateRegLabel()
        {
            if (PlayerStatus.LoggedIn)
                label_reg.Content = "Logged In";
            else if (!PlayerStatus.LoggedIn && PlayerStatus.LogInSent)
                label_reg.Content = "Logging In...";

            if (PlayerStatus.JoinedGame)
                label_reg.Content = "In Game";
            else if (PlayerStatus.JoinGameSent && !PlayerStatus.JoinedGame)
                label_reg.Content = "Joining Game...";
            
        }

        public void updateAliveRequest()
        { 
            if(PlayerStatus.AliveRequest != new DateTime())
             label_lastAliveSent.Content = "Last alive: " + PlayerStatus.AliveRequest.ToString();
        }

        public void updateGameInfo()
        {
            if (PlayerStatus.JoinedGame)
            {
                label_gameID.Visibility = System.Windows.Visibility.Visible;

                label_lifepoints.Visibility = System.Windows.Visibility.Visible;
                dataGrid_pennies.Visibility = System.Windows.Visibility.Visible;
                label_penny.Visibility = System.Windows.Visibility.Visible;
                label_gameID.Content = "Game Id: " + PlayerStatus.gameId.ToString();
                label_lifepoints.Content = "Life Points: " + PlayerStatus.lifePoints.ToString();
                dataGrid_pennies.ItemsSource = PlayerStatus.pennies;
            }
        }

        /* UI Updates End-----------------------------------------------------------*/


        /* Actions ----------------------------------------------------------------*/

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            e.Cancel = true;
           
            exitCount++;
            if (exitCount >= 2)
                Environment.Exit(0);

            label_reg.Content = "Logging out...";

            LogoutRequest message = new LogoutRequest();
            message.ConversationId = MessageNumber.Create();
            message.MessageNr = MessageNumber.Create();
            Envelope env = new Envelope(message, message.ConversationId, EndPointParser.Parse(player.endpoint));
            QueueManager.addSendEnvelopeToQueue(env);

            

            Thread.Sleep(1000);
            Environment.Exit(0);
        }
        /* End Actions -------------------------------------------------------------*/
    }
}