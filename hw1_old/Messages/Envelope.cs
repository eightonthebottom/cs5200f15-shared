﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using SharedObjects;

namespace Messages
{
    public class Envelope
    {
        public Message message;
        public MessageNumber converstationID;
        public IPEndPoint endpoint;

        public Envelope(Message message, MessageNumber converstationID, IPEndPoint endpoint)
        {
            this.message = message;
            this.converstationID = converstationID;
            this.endpoint = endpoint;
        }
    }
}
