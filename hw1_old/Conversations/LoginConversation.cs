﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Players;
using QueueManagerName;
using Controller;

namespace Conversations
{
    public class LoginConversation : Conversation
    {
        public LoginConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(LoginRequest), typeof(LoginReply) };
            stageState = new State[] { State.Not_Started, State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildLoginRequest()
        {
            LoginRequest message = new LoginRequest();
            message.SetMessageAndConversationNumbers(messageNumber, conversationId);
            message.ProcessType = ProcessInfo.ProcessType.Player;
            message.ProcessLabel = player.idenitity.Alias;
            message.Identity = new IdentityInfo();
            message.Identity = player.idenitity.Clone();
 
            return message;
        }

        private Boolean parseLoginReply(LoginReply message)
        {
            if(message.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;

            exit = false;
            while(!exit) //run until told to stop
            {
                /* advance to next state when current is completed */
                if(stageState[stage_index] == State.Complete)
                {
                    stage_index++;

                    /* we are done kill conversation */
                    if (stage_index >= stageState.Length - 1)
                        exit = true;
                }
                
                /* if this state hasnt started, start it */
                if(stageState[stage_index] == State.Not_Started)
                {
                    if(stage[stage_index] == typeof(LoginRequest))
                    {
                        Envelope envelope = new Envelope(buildLoginRequest(), conversationId, remoteEP);
                        QueueManager.addSendEnvelopeToQueue(envelope);
                        stageState[stage_index] = State.In_Process;
                    }
                }

                /* handle timeout and retries */
                else if(stageState[stage_index] == State.In_Process)
                {
                    timeout++;

                    if(timeout > 10000)
                    {
                        //retry
                        timeout = 0;
                        stage_index = 0;
                        stageState = new State[] { State.Not_Started, State.Not_Started };
                    }
                }
               
                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if(check != null)
                {
                    if (check.message.GetType() == typeof(LoginReply))
                    {
                        if(parseLoginReply(check.message as LoginReply))
                        {
                            stageState[stage_index] = State.Complete;
                            PlayerStatus.LoggedIn = true;
                            PlayerStatus.myEndpoint = ((LoginReply)check.message).ProcessInfo.EndPoint;
                        }
                        else
                        {
                            /* start over and try again */
                            stage_index = 0;
                            stageState = new State[] { State.Not_Started, State.Not_Started };
                        }
                    }
                }

                Thread.Sleep(50);
            }
        }
    }
        
}
