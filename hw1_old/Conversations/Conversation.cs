﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

using SharedObjects;
using Players;
using Messages;

namespace Conversations
{
    public enum Protocol { Single, RequestReply, RequestReplyAck};
    public enum State { Not_Started, In_Process, Complete};
    public abstract class Conversation
    {
        public Protocol protocol { get; set; }
        public Boolean finished { get; set; }
        public MessageNumber conversationId { get; set; }
        public MessageNumber messageNumber { get; set; }
        public Type[] stage { get; set; }
        public State[] stageState { get; set; }
        public IPEndPoint remoteEP { get; set; }
        public Player player { get; set; }
        public Boolean exit { get; set; }
        public int stage_index { get; set; }

        public abstract void runConversation(object o);
    }
}
