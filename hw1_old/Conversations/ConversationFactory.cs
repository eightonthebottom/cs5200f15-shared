﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using log4net;

using Players;
using SharedObjects;
using Messages;
using Messages.RequestMessages;


namespace Conversations
{
    public static class ConversationFactory
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public static Conversation create(Type type, MessageNumber conversationId,  MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            Conversation c = null;

            if(type == typeof(LoginRequest) || type == typeof(LoginReply))
            {
                c = new LoginConversation(conversationId, messageNumber, player, remoteEP);
                log.Debug("Login Conversation Started");
            }
            else if(type == typeof(AliveRequest))
            {
                c = new AliveRequestConversation(conversationId, messageNumber, player, remoteEP);
                log.Debug("Alive Request Conversation Started");
            }
            else if(type == typeof(GameListRequest) || type == typeof(GameListReply))
            {
                c = new GameListConversation(conversationId, messageNumber, player, remoteEP);
                log.Debug("Game List Conversation Started");
            }
            else if(type == typeof(JoinGameRequest) || type == typeof(JoinGameReply))
            {
                c = new JoinGameConversation(conversationId, messageNumber, player, remoteEP);
                log.Debug("Join Game Conversation Started");
            }

            return c;
        }
    }
}
