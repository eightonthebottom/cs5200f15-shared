﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using Messages;
using Messages.RequestMessages;
using SharedObjects;
using Players;
using Controller;
using QueueManagerName;

namespace Conversations
{
    public class GameListConversation : Conversation
    {
        public GameListConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(GameListRequest), typeof(GameListReply) };
            stageState = new State[] { State.Not_Started, State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildGameListRequest()
        {
            GameListRequest message = new GameListRequest();
            message.SetMessageAndConversationNumbers(messageNumber, conversationId);
            message.StatusFilter = (int)(GameInfo.StatusCode.Available | GameInfo.StatusCode.Initializing | GameInfo.StatusCode.InProgress);

            return message;
        }

        private Boolean parseGameListReply(GameListReply message)
        {
            if (message.Success)
            {
                if (message.GameInfo.Length > 0)
                {
                    PlayerStatus.gameInfo = message.GameInfo;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;

            exit = false;
            while(!exit)
            {
                /* advance to next state when current is completed */
                if (stageState[stage_index] == State.Complete)
                {
                    stage_index++;

                    /* we are done kill conversation */
                    if (stage_index >= stageState.Length - 1)
                        exit = true;
                }

                /* if this state hasnt started, start it */
                if (stageState[stage_index] == State.Not_Started)
                {
                    if (stage[stage_index] == typeof(GameListRequest))
                    {
                        Envelope envelope = new Envelope(buildGameListRequest(), conversationId, remoteEP);
                        QueueManager.addSendEnvelopeToQueue(envelope);
                        stageState[stage_index] = State.In_Process;
                    }
                }

                /* handle timeout and retries */
                else if (stageState[stage_index] == State.In_Process)
                {
                    timeout++;

                    if (timeout > 10000)
                    {
                        //retry
                        timeout = 0;
                        stage_index = 0;
                        stageState = new State[] { State.Not_Started, State.Not_Started };
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(GameListReply))
                    {
                        if (parseGameListReply(check.message as GameListReply))
                        {
                            stageState[stage_index] = State.Complete;
                            PlayerStatus.HaveGameList = true;
                        }
                        else
                        {
                            /* start over and try again */
                            stage_index = 0;
                            stageState = new State[] { State.Not_Started, State.Not_Started };
                            Thread.Sleep(500); // wait a while, there was no game so give some time for one to start
                        }
                    }
                }

                Thread.Sleep(50);
            }
        }
    }
}
