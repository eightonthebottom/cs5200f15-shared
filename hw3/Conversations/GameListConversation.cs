﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using Messages;
using Messages.RequestMessages;
using SharedObjects;
using Players;
using Controller;
using QueueManagerName;

namespace Conversations
{
    public class GameListConversation : Conversation
    {
        public GameListConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(GameListRequest), typeof(GameListReply) };
            stageState = new State[] { State.Not_Started, State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildGameListRequest()
        {
            GameListRequest message = new GameListRequest();
            message.SetMessageAndConversationNumbers(messageNumber, conversationId);
            message.StatusFilter = (int)(GameInfo.StatusCode.Available | GameInfo.StatusCode.Initializing | GameInfo.StatusCode.InProgress);

            return message;
        }

        private Boolean parseGameListReply(GameListReply message)
        {
            if (message.Success)
            {
                if (message.GameInfo.Length > 0)
                {
                    PlayerStatus.gameInfo = message.GameInfo;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;
            int reties = 0;
            Boolean sent = false;
            exit = false;
            while(!exit)
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if(reties > PlayerStatus.retries)
                {
                    break;
                }



                if (!sent)
                {
                    Envelope envelope = new Envelope(buildGameListRequest(), conversationId, remoteEP);
                    QueueManager.addSendEnvelopeToQueue(envelope);
                    sent = true;
                }
                /* handle timeout and retries */
                else
                {
                    timeout++;

                    if (timeout > (PlayerStatus.timeout / 10))
                    {
                        //retry
                        timeout = 0;
                        reties++;
                        sent = false;
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(GameListReply))
                    {
                        if (parseGameListReply(check.message as GameListReply))
                        {
                            PlayerStatus.HaveGameList = true;
                            exit = true;
                            break;
                        }
                        else
                        {
                            /* start over and try again */
                            reties++;
                            sent = false;
                        }
                    }
                }

                Thread.Sleep(10);
            }
        }
    }
}
