﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Players;
using QueueManagerName;
using Controller;

namespace Conversations
{
    public class BuyBalloonConversation : Conversation
    {
        Penny holdTillBalloonPenny = null;
        public BuyBalloonConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(BuyBalloonRequest),  typeof(Reply)};
            stageState = new State[] { State.Not_Started, State.Not_Started};
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildBalloonRequest()
        {
            BuyBalloonRequest message = new BuyBalloonRequest();
            Penny temp = new Penny();
            Penny c = null;
            temp = PlayerStatus.pennies.ElementAt(0).Value;
            PlayerStatus.pennies.TryRemove(temp.Id, out c);
            message.Penny = temp;
            if (message.Penny != null)
                holdTillBalloonPenny = message.Penny.Clone();
            else
                holdTillBalloonPenny = null;
            message.SetMessageAndConversationNumbers(messageNumber, conversationId);

            if (message.Penny == null)
                message = null;

            return message;
        }

        private Boolean parseBalloonReply(BalloonReply message)
        {
           if(message.Balloon != null)
           {
               return true;
           }
           else
           {
               return false;
           }
        }

        public override void runConversation(object o)
        {
            int timeout = 0;
            int retries = 0;
            exit = false;
            Boolean sent = false;

            while(!exit)
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if(retries > PlayerStatus.retries)
                {
                    break;
                }

                if(!sent)
                {
                    Envelope envelope = new Envelope(buildBalloonRequest(), conversationId, PlayerStatus.balloonStore);
                    QueueManager.addSendEnvelopeToQueue(envelope);
                    sent = true;
                }
        
                /* handle timeout and retries */
                else
                {
                    timeout++;

                    if (timeout > (PlayerStatus.timeout/10))
                    {
                        /* put penny back into list */
                        PlayerStatus.pennies.TryAdd(holdTillBalloonPenny.Id, holdTillBalloonPenny);
                        holdTillBalloonPenny = null;
                        //retry
                        timeout = 0;
                        retries++;
                        sent = false;
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(BalloonReply))
                    {
                        if (parseBalloonReply(check.message as BalloonReply))
                        {
                            PlayerStatus.balloons.Enqueue(((BalloonReply)check.message).Balloon);
                            holdTillBalloonPenny = null;
                            PlayerStatus.buyBalloonDone = true;
                            PlayerStatus.LifePoints = PlayerStatus.LifePoints;
                            exit = true;
                            break;
                        }
                        else
                        {
                            /* put penny back into list */
                            PlayerStatus.pennies.TryAdd(holdTillBalloonPenny.Id,holdTillBalloonPenny);
                            holdTillBalloonPenny = null;
                            retries++;
                            sent = false;
                        }
                    }
                }

                Thread.Sleep(10);
            }
            PlayerStatus.buyBalloonDone = true;
        }
    }
}
