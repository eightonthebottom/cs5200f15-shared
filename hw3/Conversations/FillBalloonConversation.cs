﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Players;
using QueueManagerName;
using Controller;

namespace Conversations
{
    public class FillBalloonConversation : Conversation
    {
        Penny holdTillBalloonPennyA = null;
        Penny holdTillBalloonPennyB = null;
        public FillBalloonConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(FillBalloonRequest), typeof(Reply)};
            stageState = new State[] { State.Not_Started, State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
            
        }

        private Message buildBalloonRequest()
        {
            FillBalloonRequest message = new FillBalloonRequest();
            Balloon temp_Balloon = null;
            PlayerStatus.balloons.TryDequeue(out temp_Balloon);
            message.Balloon = temp_Balloon;
            Penny temp_penny = new Penny();
            Penny temp2_penny = new Penny();
            Penny c = null;
            temp_penny = PlayerStatus.pennies.ElementAt(0).Value;
            PlayerStatus.pennies.TryRemove(temp_penny.Id, out c);
            temp2_penny = PlayerStatus.pennies.ElementAt(0).Value;
            PlayerStatus.pennies.TryRemove(temp_penny.Id, out c);
            message.Pennies = new Penny[2] { temp_penny, temp2_penny };

            if (message.Pennies[0] != null && message.Pennies[1] != null)
            {
                holdTillBalloonPennyA = message.Pennies[0].Clone();
                holdTillBalloonPennyB = message.Pennies[1].Clone();
            }

            message.SetMessageAndConversationNumbers(messageNumber, conversationId);

            if (message.Pennies[0] == null || message.Pennies[1] == null || message.Balloon == null)
                message = null;

            return (Message)message;
        }

        private Boolean parseBalloonReply(BalloonReply message)
        {
            if (message.Balloon != null)
            {
                if (message.Balloon.IsFilled)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
                return false;
        } 

        public override void runConversation(object o)
        {
             int timeout = 0;
             int retries = 0;
            exit = false;
            Boolean sent = false;

            while(!exit)
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if(retries > PlayerStatus.retries)
                {
                    break;
                }
                
                if(!sent)
                {
                    Envelope envelope = new Envelope(buildBalloonRequest(), conversationId, PlayerStatus.waterStore);
                    QueueManager.addSendEnvelopeToQueue(envelope);
                    sent = true;
                }
                /* handle timeout and retries */
                else
                {
                    timeout++;

                    if (timeout > (PlayerStatus.timeout/10))
                    {
                        /* put penny back into list */
                        PlayerStatus.pennies.TryAdd(holdTillBalloonPennyA.Id,holdTillBalloonPennyA);
                        PlayerStatus.pennies.TryAdd(holdTillBalloonPennyB.Id,holdTillBalloonPennyB);
                        holdTillBalloonPennyA = null;
                        holdTillBalloonPennyB = null;

                        PlayerStatus.balloons.Enqueue(new Balloon());
                        //retry
                        timeout = 0;
                        retries++;
                        sent = false;
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(BalloonReply))
                    {
                        if (parseBalloonReply(check.message as BalloonReply))
                        {
                            PlayerStatus.balloons.Enqueue(((BalloonReply)check.message).Balloon);
                            holdTillBalloonPennyA = null;
                            holdTillBalloonPennyB = null;
                            PlayerStatus.fillBalloonDone = true;
                            PlayerStatus.LifePoints = PlayerStatus.LifePoints;
                            exit = true;
                            break;
                        }
                        else
                        {
                            /* put penny back into list */
                            PlayerStatus.pennies.TryAdd(holdTillBalloonPennyA.Id,holdTillBalloonPennyA);
                            PlayerStatus.pennies.TryAdd(holdTillBalloonPennyB.Id,holdTillBalloonPennyB);
                            holdTillBalloonPennyA = null;
                            holdTillBalloonPennyB = null;
                            PlayerStatus.balloons.Enqueue(new Balloon());

                            /* start over and try again */
                            sent = false;
                        }
                    }
                }

                Thread.Sleep(10);
            }
        }
    }
}
