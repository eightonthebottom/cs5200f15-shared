﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Players;
using QueueManagerName;
using Controller;

namespace Conversations
{
    public class ThrowBalloonConversation : Conversation
    {
        Balloon holdBalloonTillSuccess = null;
        public ThrowBalloonConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(ThrowBalloonRequest),  typeof(Reply)};
            stageState = new State[] { State.Not_Started, State.Not_Started};
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildThrowRequest()
        {
            ThrowBalloonRequest message = new ThrowBalloonRequest();
            message.SetMessageAndConversationNumbers(messageNumber, conversationId);
            holdBalloonTillSuccess = null;
            PlayerStatus.balloons.TryDequeue(out holdBalloonTillSuccess);
            message.Balloon = holdBalloonTillSuccess;
            message.TargetPlayerId = PlayerStatus.lastPlayerToHitMe;

            if (message.Balloon != null)
            {
                if (!message.Balloon.IsFilled)
                {
                    message = null;
                }

                if (message != null)
                {
                    if (message.TargetPlayerId == 0)
                    {
                        //revenge first, weakest next
                        if(PlayerStatus.playersInGame.Count > 0)
                        {
                            ProcessInfo[] toKill = PlayerStatus.playersInGame.Select(pi => pi.Value).ToArray();
                            int minLife = Int32.MaxValue;
                            int minId = Int32.MaxValue;
                            foreach(ProcessInfo p in toKill)
                            {
                                if(p.LifePoints < minLife && p.LifePoints > 0)
                                {
                                    minLife = p.LifePoints;
                                    minId = p.ProcessId;
                                }
                            }

                            if (minId < Int32.MaxValue)
                                message.TargetPlayerId = minId;
                        }
                    }
                }
            }
            else
                message = null;

            return message;
        }

        private Boolean parseReply(Reply message)
        {
            if (message.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;
            int retries = 0;
            Boolean sent = false;

            exit = false;
            while (!exit) //run until told to stop
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if (retries > PlayerStatus.retries)
                {
                    PlayerStatus.throwBalloonDone = true;
                    break;
                }

                if(!sent)
                {
                    Message temp = buildThrowRequest();

                    if (temp != null)
                    {
                        Envelope envelope = new Envelope(temp, conversationId, PlayerStatus.currentGame.EndPoint.IPEndPoint);
                        QueueManager.addSendEnvelopeToQueue(envelope);
                        sent = true;
                    }
                    else
                        retries++;
                 }
        
                /* handle timeout and retries */
                else
                {
                    timeout++;

                    if (timeout > (PlayerStatus.timeout / 10))
                    {
                        //retry
                        retries++;
                        timeout = 0;
                        sent = false;
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(Reply))
                    {
                        if (parseReply(check.message as Reply))
                        {
                            holdBalloonTillSuccess = null;
                            exit = true;
                            PlayerStatus.throwBalloonDone = true;
                            break;
                        }
                        else
                        {
                            /* start over and try again */
                            PlayerStatus.balloons.Enqueue(holdBalloonTillSuccess);
                            retries++;
                            sent = false;
                        }
                    }
                }

                Thread.Sleep(10);
            }
            PlayerStatus.throwBalloonDone = true;
        }
    }
}
