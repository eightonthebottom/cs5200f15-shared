﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using Messages;
using Messages.RequestMessages;
using Players;
using SharedObjects;
using QueueManagerName;
using Controller;
using Connection_UDP_JSON;

namespace Conversations
{
    public class JoinGameConversation : Conversation
    {
        ProcessInfo gm = null;

        public JoinGameConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(JoinGameRequest), typeof(JoinGameReply) };
            stageState = new State[] { State.Not_Started, State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildJoinGameRequest()
        {
            GameInfo game = null;
            JoinGameRequest message = null;
            
            foreach(GameInfo g in PlayerStatus.gameInfo)
            {
                if(g.Status == GameInfo.StatusCode.Available)
                {
                    game = g.Clone();
                    gm = g.GameManager.Clone();
                }
            }

            if(game != null)
            {
                message = new JoinGameRequest();
                message.SetMessageAndConversationNumbers(messageNumber, conversationId);
                message.GameId = game.GameId;
                message.Player = new ProcessInfo();
                message.Player.EndPoint = PlayerStatus.myEndpoint;
                message.Player.ProcessId = conversationId.ProcessId;
                message.Player.Label = player.idenitity.Alias;
                message.Player.Type = ProcessInfo.ProcessType.Player;
            }

             return message;
        }

        private Boolean parseJoinGameReply(JoinGameReply message)
        {
            if (message.Success)
            {
                PlayerStatus.gameId = message.GameId;
                PlayerStatus.LifePoints = message.InitialLifePoints;             
                return true;
            }
            else
            {
                return false;
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;
            int reties = 0;
            Boolean sent = false;

            exit = false;
            while (!exit) //run until told to stop
            {

                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if(reties > PlayerStatus.retries)
                {
                    break;
                }

                if(!sent)
                {
                    Message m = buildJoinGameRequest(); 
                    if (m != null && gm != null)
                    {
                        Envelope envelope = new Envelope(m, conversationId, gm.EndPoint.IPEndPoint);
                        PlayerStatus.currentGame = gm.Clone();
                        QueueManager.addSendEnvelopeToQueue(envelope);
                        stageState[stage_index] = State.In_Process;
                        sent = true;
                    }
                }
                /* handle timeout and retries */
                else
                {
                    timeout++;

                    if (timeout >(PlayerStatus.timeout/10))
                    {
                        //retry
                        timeout = 0;
                        reties++;
                        PlayerStatus.currentGame = null;
                        sent = false;
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(JoinGameReply))
                    {
                        if (parseJoinGameReply(check.message as JoinGameReply))
                        {                    
                            PlayerStatus.JoinedGame = true;
                            exit = true;
                            break;
                        }
                        else
                        {
                            /* start over and try again */
                            reties++;
                            sent = false;
                            PlayerStatus.currentGame = null;
                        }
                    }
                }

                Thread.Sleep(10);
            }
        }
    }
}
