﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Players;
using QueueManagerName;
using Controller;

namespace Conversations
{
    public class HitByBalloonConversation : Conversation
    {
        public HitByBalloonConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(HitNotification) };
            stageState = new State[] { State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Message buildReply()
        {
            Reply m = new Reply();
            m.SetMessageAndConversationNumbers(conversationId);
            m.Note = "revenge shall be mine";
            m.Success = true;

            return m;
        }

         /* Main conversation loop */
        public override void runConversation(object o)
        {
            exit = false;
            while (!exit) //run until told to stop
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(HitNotification))
                    {
                        exit = true;
                        PlayerStatus.LifePoints = PlayerStatus.LifePoints-1;
                        PlayerStatus.lastPlayerToHitMe = ((HitNotification)check.message).ByPlayerId;
                        if(PlayerStatus.currentGame != null)
                            QueueManager.addEnvelopeToQueue(new Envelope(buildReply(), conversationId, PlayerStatus.currentGame.EndPoint.IPEndPoint));
                    }
                }

                Thread.Sleep(10);
            }
        }

    }
}
