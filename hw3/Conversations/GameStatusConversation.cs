﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.StatusMessages;
using Messages.StreamMessages;
using Players;
using QueueManagerName;
using Controller;


namespace Conversations
{
    public class GameStatusConversation : Conversation
    {
        private PublicEndPoint tcpPort = null;
        private GameInfo gameInfo = null;
        private int gameId = 0;
        private TcpClient tcpClient = null;
        private NetworkStream netStream = null;
        private Boolean gameBrainStarted = false;

        public GameStatusConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            stage = new Type[] { typeof(ReadyToStart), typeof(StreamMessage), typeof(EndGame) };
            stageState = new State[] { State.Not_Started, State.Not_Started, State.Not_Started };
            stage_index = 0;
            this.remoteEP = remoteEP;
        }

        private Boolean parseReadyToStartReply(ReadyToStart message)
        {
            if(message.Game != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Boolean parseEndGame(EndGame message)
        {
            if (message.GameId != gameId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void parseStreamMessage(StreamMessage message)
        {
            if(message != null)
            {
                if(message.GetType() == typeof(InGame))
                {
                    int id = (message as InGame).Process.ProcessId;
                    ProcessInfo info = (message as InGame).Process;
                    if (info.Type == ProcessInfo.ProcessType.Player)
                    {
                        if (id != PlayerStatus.myId)
                        {
                            if (PlayerStatus.playersInGame.ContainsKey(id))
                            {
                                ProcessInfo temp;
                                PlayerStatus.playersInGame.TryRemove(id, out temp);
                                PlayerStatus.playersInGame.TryAdd(id, info);
                            }
                            else
                                PlayerStatus.playersInGame.TryAdd(id, info);
                        } 
                    }
                    if (info.Type == ProcessInfo.ProcessType.BalloonStore)
                    {
                        PlayerStatus.balloonStore = info.EndPoint.IPEndPoint;
                    }
                    if(info.Type == ProcessInfo.ProcessType.WaterServer)
                    {
                        PlayerStatus.waterStore = info.EndPoint.IPEndPoint;
                    }
                }
                else if(message.GetType() == typeof(HaveAPenny))
                {
                    Penny p = (message as HaveAPenny).Penny;
                    PlayerStatus.pennies.TryAdd(p.Id,p);
                }
                else if(message.GetType() == typeof(NotInGame))
                {
                    int id = (message as NotInGame).ProcessId;
                    if (id != PlayerStatus.myId)
                    {
                        ProcessInfo temp;
                        PlayerStatus.playersInGame.TryRemove(id, out temp);
                    }
                    else
                    {
                        PlayerStatus.Reset();
                    }
                }
                else if(message.GetType() == typeof(StartGame))
                {
                    if (!gameBrainStarted)
                    {
                        GameBrain myBrain = new GameBrain();
                        Thread gameThread = new Thread(new ThreadStart(myBrain.runGamePlayer));
                        gameThread.Start();
                        gameBrainStarted = true;
                        PlayerStatus.GameStarted = true;
                        PlayerStatus.LifePoints = 100;
                    }
                }
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;
            int retries = 0;

            exit = false;
            while (!exit) //run until told to stop
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if (retries > PlayerStatus.retries)
                {
                    break;
                }

                /* advance to next state when current is completed */
                if (stageState[stage_index] == State.Complete)
                {
                    stage_index++;

                    /* we are done kill conversation */
                    if (stage_index >= stageState.Length - 1)
                    {
                        exit = true;
                        break;
                    }
                }

                /* if this state hasnt started, start it */
                if (stageState[stage_index] == State.Not_Started)
                {
                    if (stage[stage_index] == typeof(StreamMessage))
                    {
                        stageState[stage_index] = State.In_Process;
                        tcpClient = new TcpClient();
                        tcpClient.Connect(tcpPort.IPEndPoint);
                        netStream = tcpClient.GetStream();
                        netStream.ReadTimeout = 1000;
                    }
                }

                /* handle timeout and retries */
                else if (stageState[stage_index] == State.In_Process)
                {
                    if (stage[stage_index] != typeof(StreamMessage))
                    {
                        timeout++;

                        if (timeout > (PlayerStatus.timeout / 10))
                        {
                            //retry
                            retries++;
                            timeout = 0;
                            stage_index = 0;
                            stageState = new State[] { State.Not_Started, State.Not_Started, State.Not_Started };
                        }
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(ReadyToStart))
                    {
                        if (parseReadyToStartReply(check.message as ReadyToStart))
                        {
                            stageState[stage_index] = State.Complete;
                            tcpPort = (check.message as ReadyToStart).StreamEP;
                            gameInfo = (check.message as ReadyToStart).Game;
                            gameId = (check.message as ReadyToStart).GameId;
                        }
                    }
                    if (check.message.GetType() == typeof(EndGame))
                    {
                        if (parseEndGame(check.message as EndGame))
                        {
                            stageState[stage_index] = State.Complete;
                            exit = true;
                            break;
                        }
                    }
                }

                if(stageState[stage_index] == State.In_Process && stage[stage_index] == typeof(StreamMessage))
                {
                    if (netStream.CanRead)
                    {
                        byte[] bytes = new byte[4];
                        int bytesRead = 0;
                        try
                        {
                            while (netStream.CanRead && bytesRead < bytes.Length)
                                bytesRead += netStream.Read(bytes, 0, bytes.Length);
                        }
                        catch (IOException e)
                        {
                            System.Console.WriteLine(e.ToString());
                        }

                        if(bytesRead == bytes.Length)
                        {
                            int messageLength = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
                            bytes = new byte[messageLength];

                            bytesRead = 0;

                            try
                            {
                                while (netStream.CanRead && bytesRead < bytes.Length)
                                    bytesRead += netStream.Read(bytes, bytesRead, bytes.Length - bytesRead);
                            }
                            catch (IOException e)
                            {
                                System.Console.WriteLine(e.ToString());
                            }
                        }

                        if(bytesRead == bytes.Length)
                        {
                            StreamMessage message = StreamMessage.Decode(bytes);
                            parseStreamMessage(message);
                        }
                    }
                }

                Thread.Sleep(10);
            }

            if (tcpClient != null && netStream != null)
            {
                tcpClient.Close();
                netStream.Close();
            }
        }
    }
}
