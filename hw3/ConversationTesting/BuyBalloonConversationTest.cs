﻿using System;
using System.Threading;
using System.Collections.Concurrent;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Conversations;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Controller;

namespace ConversationTesting
{
    [TestClass]
    public class BuyBalloonConversationTest
    {
        [TestMethod]
        public void TestBuyBalloonNormal()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            BuyBalloonConversation myBuy = new BuyBalloonConversation(myNum, myNum, player, ep.IPEndPoint);

            PlayerStatus.pennies = new ConcurrentDictionary<int, Penny>();
            PlayerStatus.balloons = new ConcurrentQueue<Balloon>();

            PlayerStatus.pennies.TryAdd(0,new Penny());

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myBuy.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(BuyBalloonRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(10);
            BalloonReply lr = new BalloonReply();
            lr.Balloon = new Balloon();
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(10);
            Assert.AreEqual(1, PlayerStatus.balloons.Count);
            Assert.AreEqual(0, PlayerStatus.pennies.Count);

            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestBuyFail()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            BuyBalloonConversation myBuy = new BuyBalloonConversation(myNum, myNum, player, ep.IPEndPoint);

            PlayerStatus.pennies = new ConcurrentDictionary<int,Penny>();
            PlayerStatus.balloons = new ConcurrentQueue<Balloon>();

            PlayerStatus.pennies.TryAdd(0,new Penny());

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myBuy.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(BuyBalloonRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            BalloonReply lr = new BalloonReply();
            lr.Balloon = null;
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            
            Assert.AreEqual(0, PlayerStatus.balloons.Count);
            

            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(BuyBalloonRequest));
            }

            myBuy.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestBuyNoReply()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            BuyBalloonConversation myBuy = new BuyBalloonConversation(myNum, myNum, player, ep.IPEndPoint);

            PlayerStatus.pennies.TryAdd(0,new Penny());

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;
            PlayerStatus.balloons = new ConcurrentQueue<Balloon>();

            Thread t = new Thread(new ParameterizedThreadStart(myBuy.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(BuyBalloonRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            Assert.AreEqual(0, PlayerStatus.balloons.Count);

            Thread.Sleep(4000);
            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(BuyBalloonRequest));
            }
            Envelope e3 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNull(e3);

            Thread.Sleep(100);

            myBuy.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }
    }
}
