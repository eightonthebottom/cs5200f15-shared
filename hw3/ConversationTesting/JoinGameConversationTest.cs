﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

using Conversations;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Controller;

namespace ConversationTesting
{
    [TestClass]
    public class JoinGameConversationTest
    {
        [TestMethod]
        public void TestJoinNormal()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            JoinGameConversation myJoin = new JoinGameConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            GameInfo gi = new GameInfo();
            gi.Status = GameInfo.StatusCode.Available;
            gi.GameManager = new ProcessInfo();
            gi.GameManager.EndPoint = ep;
            PlayerStatus.gameInfo = new GameInfo[] { gi, new GameInfo() };

            Thread t = new Thread(new ParameterizedThreadStart(myJoin.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(JoinGameRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            JoinGameReply lr = new JoinGameReply();
            //lr.Pennies = new Penny[] {new Penny()};
            lr.Success = true;
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(100);
            Assert.IsTrue(PlayerStatus.JoinedGame);

            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestJoinGameFail()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            JoinGameConversation myJoin = new JoinGameConversation(myNum, myNum, player, ep.IPEndPoint);

            PlayerStatus.JoinedGame = false;

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;


            GameInfo gi = new GameInfo();
            gi.Status = GameInfo.StatusCode.Available;
            gi.GameManager = new ProcessInfo();
            gi.GameManager.EndPoint = ep;
            PlayerStatus.gameInfo = new GameInfo[] { gi, new GameInfo() };

            Thread t = new Thread(new ParameterizedThreadStart(myJoin.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(JoinGameRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            JoinGameReply lr = new JoinGameReply();
            //lr.Pennies = new Penny[] {new Penny()};
            lr.Success = false;
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(100);
            Assert.IsFalse(PlayerStatus.JoinedGame);

            Thread.Sleep(100);
            /*check Queue Manager*/
            e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(JoinGameRequest));
            }

            myJoin.exit = true;
            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestJoinNoReply()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            JoinGameConversation myJoin = new JoinGameConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            GameInfo gi = new GameInfo();
            gi.Status = GameInfo.StatusCode.Available;
            gi.GameManager = new ProcessInfo();
            gi.GameManager.EndPoint = ep;
            PlayerStatus.gameInfo = new GameInfo[] { gi, new GameInfo() };

            Thread t = new Thread(new ParameterizedThreadStart(myJoin.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(JoinGameRequest));
            }

            e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNull(e);
            Assert.IsFalse(PlayerStatus.JoinedGame);

            Thread.Sleep(6500);
            e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);
            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(JoinGameRequest));
            }

            myJoin.exit = true;
            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }
    }
}
