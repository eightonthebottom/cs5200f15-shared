﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;


using Conversations;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Controller;

namespace ConversationTesting
{
    [TestClass]
    public class GameListConversationTest
    {
        [TestMethod]
        public void TestGameListRequestNormal()
        {
            //MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            GameListConversation myGameList = new GameListConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myGameList.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(GameListRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            GameListReply lr = new GameListReply();
            lr.Success = true;
            GameInfo one = new GameInfo();
            GameInfo two = new GameInfo();
            lr.GameInfo = new GameInfo[] { one, two };
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(100);
            Assert.IsNotNull(PlayerStatus.gameInfo);

            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestGameListFail()
        {
            //MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP
            PlayerStatus.gameInfo = null;
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            GameListConversation myGameList = new GameListConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myGameList.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(GameListRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            GameListReply lr = new GameListReply();
            lr.Success = false;
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(100);
            Assert.IsNull(PlayerStatus.gameInfo);

            Thread.Sleep(600);

            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(GameListRequest));
            }

            myGameList.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestGameListNoReply()
        {
            PlayerStatus.gameInfo = null;
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            GameListConversation myGameList = new GameListConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myGameList.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(GameListRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            Assert.IsNull(PlayerStatus.gameInfo);

            /* sleep almost a min to make sure retry happens */
            Thread.Sleep(4000);
            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(GameListRequest));
            }
            Envelope e3 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNull(e3);

            Thread.Sleep(100);

            myGameList.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestGameListReplyNoGames()
        {
            PlayerStatus.gameInfo = null;
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            GameListConversation myGame = new GameListConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);

            Thread t = new Thread(new ParameterizedThreadStart(myGame.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(GameListRequest));
            }

            GameListReply lr = new GameListReply();
            lr.Success = true;
            lr.GameInfo = new GameInfo[0];
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);
            /* pretend wait for response */
            Thread.Sleep(100);
            Assert.IsNull(PlayerStatus.gameInfo);

            Thread.Sleep(1000);
            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(GameListRequest));
            }
            Envelope e3 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNull(e3);

            Thread.Sleep(100);

            myGame.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }
    }
}
