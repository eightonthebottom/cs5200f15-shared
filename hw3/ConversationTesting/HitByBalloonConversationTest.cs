﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

using Conversations;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Controller;

namespace ConversationTesting
{
    [TestClass]
    public class HitByBalloonConversationTest
    {
        [TestMethod]
        public void TestJoinNormal()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            HitByBalloonConversation myHit = new HitByBalloonConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            HitNotification message = new HitNotification();
            message.ByPlayerId = 10;
            message.ConversationId = myNum;
            message.MessageNr = myNum;
            Envelope e = new Envelope(message, myNum, ep.IPEndPoint);

            QueueManager.addEnvelopeToQueue(e);

            PlayerStatus.LifePoints = 100;

            Thread.Sleep(50);
            Thread t = new Thread(new ParameterizedThreadStart(myHit.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNull(e);

            Assert.AreEqual(99, PlayerStatus.LifePoints);

            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }
    }
}
