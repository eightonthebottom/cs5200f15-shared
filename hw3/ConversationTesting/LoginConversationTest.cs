﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Conversations;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Controller;

namespace ConversationTesting
{
    [TestClass]
    public class LoginConversationTest
    {
        [TestMethod]
        public void TestLoginNormal()
        {
            //MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            LoginConversation myLogin = new LoginConversation(myNum, myNum, player, ep.IPEndPoint);
            
            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);

            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myLogin.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if(e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(LoginRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            LoginReply lr = new LoginReply();
            lr.Success = true;
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(100);
            Assert.IsTrue(PlayerStatus.LoggedIn);

            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestLoginFail()
        {
            //MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP
            PlayerStatus.LogInSent = false;
            PlayerStatus.LoggedIn = false;
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            LoginConversation myLogin = new LoginConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myLogin.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(LoginRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            LoginReply lr = new LoginReply();
            lr.Success = false;
            e = new Envelope(lr, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(100);
            Assert.IsFalse(PlayerStatus.LoggedIn);

            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(LoginRequest));
            }

            myLogin.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }

        [TestMethod]
        public void TestLoginNoReply()
        {
            //MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP
            PlayerStatus.LogInSent = false;
            PlayerStatus.LoggedIn = false; ;
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            LoginConversation myLogin = new LoginConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myLogin.runConversation));
            t.Start(null);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(LoginRequest));
            }

            /* pretend wait for response */
            Thread.Sleep(100);
            Assert.IsFalse(PlayerStatus.LoggedIn);

            Thread.Sleep(4000);
            /*check Queue Manager*/
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e2);

            if (e2 != null)
            {
                Assert.AreEqual(e2.converstationID.ProcessId, 111);
                Assert.AreEqual(e2.message.GetType(), typeof(LoginRequest));
            }
            Envelope e3 = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNull(e3);

            Thread.Sleep(100);

            myLogin.exit = true;
            Thread.Sleep(500);
            Assert.IsFalse(t.IsAlive);
        }
    }
}
