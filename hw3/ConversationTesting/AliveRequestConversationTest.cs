﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;

using Conversations;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Controller;

namespace ConversationTesting
{
    [TestClass]
    public class AliveRequestConversationTest
    {
        [TestMethod]
        public void TestAliveNormal()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            AliveRequestConversation myAlive = new AliveRequestConversation(myNum, myNum, player, ep.IPEndPoint);

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myAlive.runConversation));
            t.Start(null);

            AliveRequest m = new AliveRequest();
            Envelope temE = new Envelope(m, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(temE);

            /* Wait for it */
            Thread.Sleep(100);

            /*check Queue Manager*/
            Envelope e = QueueManager.getSendEnvelopeFromQueue();
            Assert.IsNotNull(e);

            if (e != null)
            {
                Assert.AreEqual(e.converstationID.ProcessId, 111);
                Assert.AreEqual(e.message.GetType(), typeof(Reply));
            }

            Assert.IsNotNull(PlayerStatus.AliveRequest);

            Thread.Sleep(100);
            Assert.IsFalse(t.IsAlive);
        }
    }
}
