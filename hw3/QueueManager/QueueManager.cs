﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using Messages;
using SharedObjects;

namespace QueueManagerName
{
    public static class QueueManager
    {
        private static ConcurrentQueue<Envelope> defaultQueue;
        private static ConcurrentQueue<Envelope> sendQueue;
        private static ConcurrentDictionary<MessageNumber, ConcurrentQueue<Envelope>> dictionaryQueue;
        private static Boolean initated = false;

        public static void Init()
        {
            if (!initated) //stop the queues from clearing out
            {
                defaultQueue = new ConcurrentQueue<Envelope>();
                sendQueue = new ConcurrentQueue<Envelope>();
                dictionaryQueue = new ConcurrentDictionary<MessageNumber, ConcurrentQueue<Envelope>>();
                initated = true;
            }
        }

        public static void addSendEnvelopeToQueue(Envelope envelope)
        {
            if (envelope != null)
            {
                sendQueue.Enqueue(envelope);
            }
        }

        public static Envelope getSendEnvelopeFromQueue()
        {
            Envelope envelope = null;
            sendQueue.TryDequeue(out envelope);

            return envelope;
        }

        public static void addEnvelopeToQueue(Envelope envelope)
        {
            if (envelope != null)
            {
                if (envelope.converstationID != null)
                {
                    if (dictionaryQueue.ContainsKey(envelope.converstationID))
                    {
                        ConcurrentQueue<Envelope> queue = null;
                        dictionaryQueue.TryGetValue(envelope.converstationID, out queue);

                        if (queue != null)
                        {
                            queue.Enqueue(envelope);
                        }
                        else
                        {
                            //not sure what best catch for bad queue return would be
                        }
                    }
                    else
                    {
                        defaultQueue.Enqueue(envelope);
                    }
                }
            }
        }

        public static Envelope getMessageDefaultQueue()
        {
            Envelope goingBack = null;
            defaultQueue.TryDequeue(out goingBack);
            return goingBack;
        }

        public static Envelope getMessageByConversationId(MessageNumber conversationId)
        {
            ConcurrentQueue<Envelope> catchQ = null;
            if (conversationId != null)
            {
                dictionaryQueue.TryGetValue(conversationId, out catchQ);
                if (catchQ != null)
                {
                    Envelope goingBack = null;
                    catchQ.TryDequeue(out goingBack);
                    return goingBack;
                }
            }
            return null;
        }

        public static void addNewQueueToDictionary(MessageNumber converstationId)
        {
            if (converstationId != null)
            {
                dictionaryQueue.TryAdd(converstationId, new ConcurrentQueue<Envelope>());
            }
        }
    }
}
