﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using SharedObjects;

namespace Players
{
    [DataContract]
    public class Player
    {
        [DataMember]
        public IdentityInfo idenitity { get; set; }
        [DataMember]
        public String endpoint { get; set; }

        public Player()
        {
            this.idenitity = new IdentityInfo();
            endpoint = "";
            idenitity.ANumber = "";
            idenitity.FirstName = "";
            idenitity.LastName = "";
            idenitity.Alias = "";
        }

        public Player(String endpoint, String firstName, String lastName, String aNumber, String alias)
        {
            this.idenitity = new IdentityInfo();

            this.endpoint = endpoint;
            this.idenitity.FirstName = firstName;
            this.idenitity.LastName = lastName;
            this.idenitity.ANumber = aNumber;
            this.idenitity.Alias = alias;
        }
    }
}

