﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Security.Cryptography;


using SharedObjects;

namespace Controller
{
    public static class PlayerStatus
    {
        public static byte[] MyKeyExponent;
        public static byte[] MyKeyModulus;
        public static ConcurrentDictionary<int, byte[]> PublicKeyExponents = new ConcurrentDictionary<int, byte[]>();
        public static ConcurrentDictionary<int, byte[]> PublicKeyModuluss = new ConcurrentDictionary<int, byte[]>();
        public static int balloonCount = 0;
        public static Boolean haveKeys = false;
        public static ConcurrentQueue<Penny> used_pennies = new ConcurrentQueue<Penny>();
        public static RSACryptoServiceProvider balloon_RSA = new RSACryptoServiceProvider();
        public static RSAPKCS1SignatureFormatter myDigitalSignatureCreator = new RSAPKCS1SignatureFormatter(balloon_RSA);
        public static RSAParameters balloon_keyInfo = new RSAParameters();

        public static Boolean buyBalloon = false;
        public static Boolean buyBalloonDone = true;
        public static Boolean fillBalloon = false;
        public static Boolean fillBalloonDone = true;
        public static Boolean throwBalloon = false;
        public static Boolean throwBalloonDone = true;

        public static PublicEndPoint myEndpoint { get; set; }
        private static Boolean loggedIn = false;
        private static Boolean logInSent = false;
        private static Boolean haveGameList = false;
        private static Boolean gameListSent = false;
        private static Boolean joinGameSent = false;
        private static Boolean joinedGame = false;
        private static Boolean gameStarted = false;
        private static DateTime aliveRequest = new DateTime();
        private static int lifePoints = 0;

        public static Boolean LoggedIn { get { return loggedIn; } set { loggedIn = value; OnChanged(EventArgs.Empty); } }
        public static Boolean LogInSent { get { return logInSent; } set { logInSent = value; OnChanged(EventArgs.Empty); } }
        public static Boolean HaveGameList { get { return haveGameList; } set { haveGameList = value; OnChanged(EventArgs.Empty); } }
        public static Boolean GameListSent { get { return gameListSent; } set { gameListSent = value; OnChanged(EventArgs.Empty); } }
        public static Boolean JoinGameSent { get { return joinGameSent; } set { joinGameSent = value; OnChanged(EventArgs.Empty); } }
        public static Boolean JoinedGame { get { return joinedGame; } set { joinedGame = value; OnChanged(EventArgs.Empty); } }
        public static DateTime AliveRequest { get { return aliveRequest; } set { aliveRequest = value; OnChanged(EventArgs.Empty); } }
        public static Boolean GameStarted { get { return gameStarted; } set { gameStarted = value; OnChanged(EventArgs.Empty); } }
        public static int LifePoints { get { return lifePoints; } set { lifePoints = value; OnChanged(EventArgs.Empty); } }

        public static Boolean loggedOut = false;
        public static GameInfo[] gameInfo = null;
        public static ProcessInfo currentGame = null;
        public static IPEndPoint waterStore = null;
        public static IPEndPoint balloonStore = null;
        public static int gameId = 0;
        public static int lastPlayerToHitMe = 0;
        public static ConcurrentDictionary<int, ProcessInfo> playersInGame = new ConcurrentDictionary<int, ProcessInfo>();
        public static ConcurrentDictionary<int, Penny> pennies = new ConcurrentDictionary<int, Penny>();
        public static ConcurrentQueue<Balloon> balloons = new ConcurrentQueue<Balloon>();

        public static int timeout = 0;
        public static int retries = 0;

        public static int myId = 0;

        public delegate void ChangedEventHandler(EventArgs e);

        public static event ChangedEventHandler Changed;

        // Invoke the Changed event; called whenever list changes
        public static void OnChanged(EventArgs e)
        {
            if (Changed != null)
                Changed(e);
        }

        public static void Reset()
        {
            loggedOut = false;
            gameInfo = null;
            currentGame = null;
            waterStore = null;
            balloonStore = null;
            gameId = 0;
            lastPlayerToHitMe = 0;
            playersInGame = new ConcurrentDictionary<int, ProcessInfo>();
            pennies = new ConcurrentDictionary<int,Penny>();
            balloons = new ConcurrentQueue<Balloon>();
            buyBalloon = false;
            buyBalloonDone = true;
            fillBalloon = false;
            fillBalloonDone = true;
            throwBalloon = false;
            throwBalloonDone = true;
            haveGameList = false;
            gameListSent = false;
            joinGameSent = false;
            joinedGame = false;
            gameStarted = false;
            lifePoints = 0;
        }
    }
}
