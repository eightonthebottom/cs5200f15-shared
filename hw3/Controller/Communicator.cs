﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net.Sockets;
using System.Net;

using Connection_UDP_JSON;
using Messages;

namespace Controller
{
    public class Communicator
    {
        private static Receiver myReceiver;
        private static Sender mySender;
        private static UdpClient myClient;
        private IPEndPoint localEP;

        public Communicator()
        {
            localEP = new IPEndPoint(IPAddress.Any, 0);
            myClient = new UdpClient(localEP);
            localEP = myClient.Client.LocalEndPoint as IPEndPoint;

            myReceiver = new Receiver(myClient, localEP);
            mySender = new Sender(myClient);

            Thread sendThread = new Thread(new ThreadStart(mySender.waitToSend));
            sendThread.Start();

            Thread.Sleep(10);

            Thread receiverThread = new Thread(new ThreadStart(myReceiver.receiveMessages));
            receiverThread.Start();
        }

        public void sendEnvelope(Envelope envelope)
        {
            mySender.addEnvelopeToSendQueue(envelope);
        }

        public Envelope getEnvelope()
        {
            return myReceiver.getNextMessage();
        }

        public void kill()
        {
            mySender.setExit(true);
            myReceiver.setExit(true);
        }

        public UdpClient getClient() { return myClient; }
        public Receiver getReceiver() { return myReceiver; }
        public Sender getSender() { return mySender; }
        public IPEndPoint getEndpoint() { return localEP; }
    }
}
