﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using SharedObjects;

namespace Controller
{
    public class GameBrain
    {
        public Boolean exit = false;
        public GameBrain()
        {
            exit = false;
        }

        public void runGamePlayer()
        {
            while(!exit)
            {
                if(PlayerStatus.loggedOut)
                {
                    exit = true;
                    break;
                }

                //buy balloon if needed
                if(PlayerStatus.balloons.Count < 3)
                {
                    if(PlayerStatus.pennies.Count > 2)
                    {
                        if (PlayerStatus.buyBalloonDone)
                        {
                            PlayerStatus.buyBalloon = true;
                            PlayerStatus.buyBalloonDone = false;
                        }
                   }
                }

                Balloon myBalloon = null;
                PlayerStatus.balloons.TryPeek(out myBalloon);
                //fill balloon if needed
                if (myBalloon != null)
                {
                    if (!myBalloon.IsFilled)
                    {
                        if (PlayerStatus.pennies.Count > 1)
                        {
                            if (PlayerStatus.buyBalloonDone && PlayerStatus.fillBalloonDone)
                            {
                                PlayerStatus.fillBalloon = true;
                                PlayerStatus.fillBalloonDone = false;
                            }
                        }
                    }
                    //throw balloon
                    else
                    {
                        if (PlayerStatus.throwBalloonDone)
                        {
                            PlayerStatus.throwBalloonDone = false;
                            PlayerStatus.throwBalloon = true;
                        }
                    }
                }

                Thread.Sleep(5);
            }
        }
    }
}
