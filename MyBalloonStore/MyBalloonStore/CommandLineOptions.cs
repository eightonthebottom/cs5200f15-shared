﻿//
// Command Line Library: Program.cs
//
// Author:
//   Giacomo Stelluti Scala (gsscoder@gmail.com)
//
// Copyright (C) 2005 - 2013 Giacomo Stelluti Scala
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using CommandLine;
using CommandLine.Text;


namespace MyBalloonStore
{
    /// <summary>
    /// Run-time options class
    /// 
    /// Instances of this class hold runtime options parsed from command-line arguments and defaulted by properties.
    /// This class uses the Option attributed in CommandLine library.
    /// </summary>
    class CommandLineOptions 
    {
        [Option("registry", MetaValue = "STRING", Required = false, HelpText = "Registry's end point")]
        public string Registry { get; set; }

        [Option("gmid", MetaValue = "INT", Required = true, HelpText = "Game Manager Process Id")]
        public int gmid { get; set; }

        [Option("gmep", MetaValue = "STRING", Required = true, HelpText = "Host:Port")]
        public string gmep { get; set; }

        [Option("gameid", MetaValue = "INT", Required = true, HelpText = "Game id")]
        public int gameid { get; set; }

        [Option("balloons", MetaValue = "INT", Required = true, HelpText = "Number of Balloons")]
        public int numBalloons { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        public void SetDefaults()
        {
            gmid = 0;
            gmep = "";
            gameid = 0;
            numBalloons = 0;
        }
    }
}