﻿using System;
using System.Windows;
using System.Threading;
using System.Windows.Threading;
using System.Configuration;
using log4net;
using log4net.Util;

using Players;
using ConversationManagerName;
using Controller;
using SharedObjects;
using CommandLine;
using Messages;
using Messages.RequestMessages;
using QueueManagerName;
using Connection_UDP_JSON;

namespace MyBalloonStore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(String));
        private CommandLineOptions options;
        private int exitCount = 0;
        private Player player;
        private ConversationManager m;

        public MainWindow()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();

            GetCommandArgs();
            int timeout = Int32.Parse(ConfigurationManager.AppSettings["Timeout"]);
            int retries = Int32.Parse(ConfigurationManager.AppSettings["Retries"]);
            initUIcontent();

            player = new Player(
                ConfigurationManager.AppSettings["Reg_Endpoint"],
                ConfigurationManager.AppSettings["First_Name"],
                ConfigurationManager.AppSettings["Last_Name"],
                ConfigurationManager.AppSettings["A_Number"], 
                ConfigurationManager.AppSettings["Alias"]
                );
            m = new ConversationManager(player);

            PlayerStatus.timeout = timeout;
            PlayerStatus.retries = retries;
            PlayerStatus.currentGame = new ProcessInfo();
            PlayerStatus.currentGame.EndPoint = new PublicEndPoint(options.gmep);
            PlayerStatus.currentGame.ProcessId = options.gmid;
            PlayerStatus.gameId = options.gameid;
            PlayerStatus.balloonCount = options.numBalloons;

            PlayerStatus.myDigitalSignatureCreator.SetHashAlgorithm("SHA1");
            PlayerStatus.balloon_keyInfo = PlayerStatus.balloon_RSA.ExportParameters(false);
            PlayerStatus.MyKeyExponent = PlayerStatus.balloon_keyInfo.Exponent;
            PlayerStatus.MyKeyModulus = PlayerStatus.balloon_keyInfo.Modulus;

            PlayerStatus.Changed += delegate(EventArgs e)
            {
                Dispatcher.BeginInvoke(new Action(() => { updateAliveRequest(); }));
                Dispatcher.BeginInvoke(new Action(() => { updateRegLabel(); }));
                Dispatcher.BeginInvoke(new Action(() => { updateGameInfo(); }));
            };
            
        }

        private void initUIcontent()
        {
            Label_Alive.Visibility = System.Windows.Visibility.Hidden;
            Label_Balloons.Content = "Balloons: " + options.numBalloons;
            Label_GameId.Content = "Game Id: " + options.gameid;
            Label_GM.Content = "Game Manager: " + options.gmid;
            Label_HP.Content = "GM Host:Post: " + options.gmep;
        }

        /* UI Updates --------------------------------------------------------------*/
        public void updateRegLabel()
        {
            if (PlayerStatus.LoggedIn)
                Label_Status.Content = "Logged In";
            else if (!PlayerStatus.LoggedIn && PlayerStatus.LogInSent)
                Label_Status.Content = "Logging In...";

            if (PlayerStatus.JoinedGame)
                Label_Status.Content = "In Game";
            else if (PlayerStatus.JoinGameSent && !PlayerStatus.JoinedGame)
                Label_Status.Content = "Joining Game...";

            if (PlayerStatus.GameStarted)
                Label_Status.Content = "Playing Game";
        }

        public void updateAliveRequest()
        {
            if (PlayerStatus.AliveRequest != new DateTime())
                Label_Alive.Content = "Last alive: " + PlayerStatus.AliveRequest.ToString();
        }

        public void updateGameInfo()
        {
            if (PlayerStatus.JoinedGame)
            {
                Label_Balloons.Content = "Balloons: " + PlayerStatus.balloonCount;
            }
        }

        /* UI Updates End-----------------------------------------------------------*/

        private void GetCommandArgs()
        {
            CommandLine.Parser parser = new CommandLine.Parser(with => with.HelpWriter = Console.Error);
            string[] args = Environment.GetCommandLineArgs();
            options = new CommandLineOptions();
            if (!parser.ParseArgumentsStrict(args, options, () => Environment.Exit(-2))) 
            {
                options.SetDefaults();
                log.Debug("Command Line Parse Failed, defaults used");
            }
            log.Debug("Command line args:");
            log.Debug("GM ID = " + options.gmid);
            log.Debug("GM Host:Port = " + options.gmep);
            log.Debug("Game Id = " + options.gameid);
            log.Debug("Balloons = " + options.numBalloons);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;

            exitCount++;
            if (exitCount >= 2)
                Environment.Exit(0);

            //label_reg.Content = "Logging out...";

            LogoutRequest message = new LogoutRequest();
            message.ConversationId = MessageNumber.Create();
            message.MessageNr = MessageNumber.Create();
            Envelope env = new Envelope(message, message.ConversationId, EndPointParser.Parse(player.endpoint));
            QueueManager.addSendEnvelopeToQueue(env);

            LeaveGameRequest message2 = new LeaveGameRequest();
            message2.ConversationId = MessageNumber.Create();
            message2.MessageNr = MessageNumber.Create();
            Envelope env2 = new Envelope(message2, message.ConversationId, PlayerStatus.currentGame.EndPoint.IPEndPoint);
            QueueManager.addSendEnvelopeToQueue(env2);

            Thread.Sleep(500);
            m.kill();
            PlayerStatus.loggedOut = true;
            Thread.Sleep(500);
            Environment.Exit(0);
        }
    }
}
