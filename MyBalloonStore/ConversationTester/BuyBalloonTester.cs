﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ConversationManagerName;
using SharedObjects;
using Players;
using QueueManagerName;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Controller;
using System.Collections.Concurrent;
using System.Threading;
using System.Security.Cryptography;

namespace ConversationTester
{
    [TestClass]
    public class BuyBalloonTester
    {
        [TestMethod]
        public void Test_BuyBalloon()
        {
            MessageNumber.LocalProcessId = 111;
            MessageNumber myNum = MessageNumber.Create();
            Player player = new Player();
            PublicEndPoint ep = new PublicEndPoint("localhost:12001");
            BuyBalloonConversationBS myBuy = new BuyBalloonConversationBS(myNum, myNum, player, ep.IPEndPoint);


            ProcessInfo pi = new ProcessInfo();
            pi.EndPoint = ep;
            PlayerStatus.playersInGame.TryAdd(111, pi);
            PlayerStatus.pennies = new ConcurrentDictionary<int,Penny>();
            PlayerStatus.balloons = new ConcurrentQueue<Balloon>();
            PlayerStatus.balloonCount = 5;
            PlayerStatus.haveKeys = true;

            QueueManager.Init();
            QueueManager.addNewQueueToDictionary(myNum);
            PlayerStatus.retries = 3;
            PlayerStatus.timeout = 3000;

            Thread t = new Thread(new ParameterizedThreadStart(myBuy.runConversation));
            t.Start(null);

            BuyBalloonRequest message = new BuyBalloonRequest();
            message.SetMessageAndConversationNumbers(myNum);

            RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider();
            RSAPKCS1SignatureFormatter myDigitalSignatureCreator = new RSAPKCS1SignatureFormatter(myRSA);
            myDigitalSignatureCreator.SetHashAlgorithm("SHA1");
            RSAParameters keyInfo = new RSAParameters();
            keyInfo = myRSA.ExportParameters(true);
            PlayerStatus.PublicKeyExponents.TryAdd(111, keyInfo.Exponent);
            PlayerStatus.PublicKeyModuluss.TryAdd(111, keyInfo.Modulus);
            PlayerStatus.currentGame = new ProcessInfo();
            PlayerStatus.currentGame.ProcessId = 111;
            SHA1Managed hasher = new SHA1Managed();
            message.Penny = new Penny();
            byte[] message_b = message.Penny.ComputeHash();
            byte[] message_hash = hasher.ComputeHash(message_b);

            message.Penny.DigitalSignature = myDigitalSignatureCreator.CreateSignature(message_hash);

            RSAPKCS1SignatureDeformatter rsaSignComparer = new RSAPKCS1SignatureDeformatter(myRSA);
            rsaSignComparer.SetHashAlgorithm("SHA1");

            byte[] messageBytes = message_b;
            byte[] messageHash = hasher.ComputeHash(messageBytes);

            Assert.IsTrue(rsaSignComparer.VerifySignature(messageHash, message.Penny.DigitalSignature));

            PlayerStatus.pennies.TryAdd(message.Penny.Id, message.Penny);
            Envelope e = new Envelope(message, myNum, ep.IPEndPoint);
            QueueManager.addEnvelopeToQueue(e);

            Thread.Sleep(500);
            Envelope e2 = QueueManager.getSendEnvelopeFromQueue();
            BalloonReply b = e2.message as BalloonReply;
            Assert.IsNotNull(e2);

            RSACryptoServiceProvider myRSA2 = new RSACryptoServiceProvider();
            RSAParameters keyInfo2 = new RSAParameters();
            keyInfo2.Modulus = PlayerStatus.MyKeyModulus;
            keyInfo2.Exponent = PlayerStatus.MyKeyExponent;
            myRSA2.ImportParameters(keyInfo2);
            SHA1Managed hasher2 = new SHA1Managed();
            RSAPKCS1SignatureDeformatter rsaSignComparer2 = new RSAPKCS1SignatureDeformatter(myRSA2);
            rsaSignComparer2.SetHashAlgorithm("SHA1");

            messageBytes = b.Balloon.ComputeHash();
            messageHash = hasher.ComputeHash(messageBytes);

            Assert.IsTrue(rsaSignComparer2.VerifySignature(messageHash, b.Balloon.DigitalSignature));
        }
    }
}
