﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using log4net;

using Players;
using Controller;
using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Messages.StreamMessages;
using Messages.StatusMessages;

namespace ConversationManagerName
{
    public static class ConversationFactoryBS
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public static Conversation create(Type type, MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            Conversation c = null;

            if (type == typeof(LoginRequest) || type == typeof(LoginReply))
            {
                c = new LoginConversationBS(conversationId, messageNumber, player, remoteEP);
                log.Debug("Login Conversation Started");
            }
            else if (type == typeof(AliveRequest))
            {
                c = new AliveRequestConversationBS(conversationId, messageNumber, player, remoteEP);
                log.Debug("Alive Request Conversation Started");
            }
            else if (type == typeof(JoinGameRequest) || type == typeof(JoinGameReply))
            {
                c = new JoinGameConversationBS(conversationId, messageNumber, player, remoteEP);
                log.Debug("Join Game Conversation Started");
            }
            else if (type == typeof(BuyBalloonRequest) || type == typeof(BalloonReply))
            {
                c = new BuyBalloonConversationBS(conversationId, messageNumber, player, remoteEP);
                log.Debug("Buy Balloon Store Conversation Started");
            }
            else if (type == typeof(ReadyToStart))
            {
                c = new GameStatusConversation(conversationId, messageNumber, player, remoteEP);
                log.Debug("Game Status Conversation Started");
            }
            else if (type == typeof(ShutdownRequest))
            {
                PlayerStatus.loggedOut = true;
                Thread.Sleep(1000);
                Environment.Exit(0);
            }

            return c;
        }
    }
}