﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Security.Cryptography;
using log4net;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.ReplyMessages;
using Players;
using QueueManagerName;
using Controller;

namespace ConversationManagerName
{
    public class BuyBalloonConversationBS : Conversation
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public BuyBalloonConversationBS(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            this.remoteEP = remoteEP;
        }

        private Balloon createBalloon()
        {
            Balloon b = null;
            
            if(PlayerStatus.balloonCount > 0 && PlayerStatus.haveKeys)
            {
                b = new Balloon();
                PlayerStatus.balloonCount--;
                SHA1Managed hasher = new SHA1Managed();
                byte[] messageBytes = b.ComputeHash();
                byte[] messageHash = hasher.ComputeHash(messageBytes);

                b.DigitalSignature = PlayerStatus.myDigitalSignatureCreator.CreateSignature(messageHash);
                log.Debug("Balloon Created");
            }
       
            return b;
        }

        private Message buildBalloonReply()
        {
            BalloonReply message = new BalloonReply();
            message.Balloon = createBalloon();
            message.SetMessageAndConversationNumbers(conversationId);

            if (message.Balloon != null)
                message.Success = true;
            else
            {
                message.Success = false;
                message.Note = "Sold out";
            }

            return message;
        }

        private Boolean parseBalloonRequest(BuyBalloonRequest message)
        {
            Boolean answer = true;

            if (PlayerStatus.playersInGame.ContainsKey(message.ConversationId.ProcessId) && PlayerStatus.haveKeys)
            {
                if (message.Penny != null)
                {

                    log.Debug("Checking penny");
                    RSACryptoServiceProvider penny_RSA = new RSACryptoServiceProvider();
                    RSAParameters penny_keyInfo = new RSAParameters();
                    PlayerStatus.PublicKeyExponents.TryGetValue(PlayerStatus.currentGame.ProcessId, out penny_keyInfo.Exponent);
                    PlayerStatus.PublicKeyModuluss.TryGetValue(PlayerStatus.currentGame.ProcessId, out penny_keyInfo.Modulus);
                    log.Debug("GM Exponent: " + FormatByteArray(penny_keyInfo.Exponent));
                    log.Debug("GM Modulus: " + FormatByteArray(penny_keyInfo.Modulus));

                    penny_RSA.ImportParameters(penny_keyInfo);
                    RSAPKCS1SignatureDeformatter rsaSignComparer = new RSAPKCS1SignatureDeformatter(penny_RSA);
                    rsaSignComparer.SetHashAlgorithm("SHA1");
                    SHA1Managed hasher = new SHA1Managed();
                    byte[] messageBytes = message.Penny.ComputeHash();
                    byte[] messageHash = hasher.ComputeHash(messageBytes);

                    if (!rsaSignComparer.VerifySignature(messageHash, message.Penny.DigitalSignature))
                    {
                        PlayerStatus.used_pennies.Enqueue(message.Penny.Clone());
                        log.Debug("Valid penny");
                        answer = true;
                    }
                    else
                    {
                        log.Debug("Bad penny");
                        answer = false;
                    }
                        
                    
                }
                else
                {
                    log.Debug("Penny in null");
                    answer = false;
                }
            }
            return answer;
        }

        private string FormatByteArray(byte[] data)
        {
            string result = "";
            foreach (byte d in data)
                result += string.Format("{0} ", d);
            return result;
        }

        public override void runConversation(object o)
        {
            int retries = 0;
            exit = false;

            while (!exit)
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if (retries > PlayerStatus.retries)
                {
                    break;
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(BuyBalloonRequest))
                    {
                        log.Debug("Buy Balloon Request Received in Conversation");
                        if (parseBalloonRequest(check.message as BuyBalloonRequest))
                        {
                            ProcessInfo temp = null;
                            PlayerStatus.playersInGame.TryGetValue(check.converstationID.ProcessId, out temp);
                            if (temp != null)
                            {
                                log.Debug("Reply Sent");
                                Envelope envelope = new Envelope(buildBalloonReply(), conversationId, temp.EndPoint.IPEndPoint);
                                QueueManager.addSendEnvelopeToQueue(envelope);
                            }
                            exit = true;
                            break;
                        }
                    }
                    else
                    {
                        /* wrong message type something very wrong */
                        break;
                    }
                }

                Thread.Sleep(10);
            }
        }
    }
}
