﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Players;
using Messages;
using Messages.RequestMessages;
using QueueManagerName;
using Connection_UDP_JSON;
using Controller;

namespace ConversationManagerName
{
    public class AliveRequestConversationBS : Conversation
    {
        public AliveRequestConversationBS(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            this.remoteEP = remoteEP;
        }

        private Message buildReply()
        {
            Reply m = new Reply();
            m.Success = true;
            m.Note = "i am still here";
            return m;
        }

        public override void runConversation(object o)
        {
            exit = false;
            while (!exit)
            {

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(AliveRequest))
                    {
                        Message m = buildReply();
                        m.ConversationId = conversationId;
                        m.MessageNr = MessageNumber.Create();
                        Envelope e = new Envelope(m, conversationId, EndPointParser.Parse(player.endpoint));

                        QueueManager.addSendEnvelopeToQueue(e);
                        PlayerStatus.AliveRequest = DateTime.Now;

                        /* conversation is a once off kill after sent */
                        //a failure to send or message lost will be handled with a new conversation
                        exit = true;
                    }
                }

                Thread.Sleep(10);
            }
        }
    }
}
