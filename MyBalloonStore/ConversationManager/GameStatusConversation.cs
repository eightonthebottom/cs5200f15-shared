﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using log4net;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.StatusMessages;
using Messages.StreamMessages;
using Players;
using QueueManagerName;
using Controller;

namespace ConversationManagerName
{
    public class GameStatusConversation : Conversation
    {
        private PublicEndPoint tcpPort = null;
        private GameInfo gameInfo = null;
        private int gameId = 0;
        private TcpClient tcpClient = null;
        private NetworkStream netStream = null;
        private Boolean gameBrainStarted = false;
        public Type[] stage { get; set; }
        public State[] stageState { get; set; }

        private static readonly ILog log = LogManager.GetLogger(typeof(String));

        public GameStatusConversation(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            this.remoteEP = remoteEP;
            stage = new Type[] { typeof(ReadyToStart), typeof(StreamMessage), typeof(EndGame) };
            stageState = new State[] { State.Not_Started, State.Not_Started, State.Not_Started };
            stage_index = 0;
        }

        private Boolean parseReadyToStartReply(ReadyToStart message)
        {
            log.Debug("Message Parsed: Ready To Start");
            if (message.Game != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private Boolean parseEndGame(EndGame message)
        {
            log.Debug("Message Parsed: End Game");
            if (message.GameId != gameId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void parseStreamMessage(StreamMessage message)
        {
            if (message != null)
            {
                if (message.GetType() == typeof(InGame))
                {
                    log.Debug("Stream Message: In Game");
                    int id = (message as InGame).Process.ProcessId;
                    ProcessInfo info = (message as InGame).Process;
                    if (info.Type == ProcessInfo.ProcessType.Player)
                    {
                        if (id != PlayerStatus.myId)
                        {
                            if (PlayerStatus.playersInGame.ContainsKey(id))
                            {
                                ProcessInfo temp;
                                PlayerStatus.playersInGame.TryRemove(id, out temp);
                                PlayerStatus.playersInGame.TryAdd(id, info);
                            }
                            else
                                PlayerStatus.playersInGame.TryAdd(id, info);
                        }
                    }
                    if (info.Type == ProcessInfo.ProcessType.BalloonStore)
                    {
                        PlayerStatus.balloonStore = info.EndPoint.IPEndPoint;
                    }
                    if (info.Type == ProcessInfo.ProcessType.WaterServer)
                    {
                        PlayerStatus.waterStore = info.EndPoint.IPEndPoint;
                    }
                }
                else if (message.GetType() == typeof(HaveAPenny))
                {
                    log.Debug("Stream Message: have penny");
                    Penny p = (message as HaveAPenny).Penny;
                    PlayerStatus.pennies.TryAdd(p.Id,p);
                }
                else if (message.GetType() == typeof(NotInGame))
                {
                    log.Debug("Stream Message: not in game");
                    int id = (message as NotInGame).ProcessId;
                    if (id != PlayerStatus.myId)
                    {
                        ProcessInfo temp;
                        PlayerStatus.playersInGame.TryRemove(id, out temp);
                    }
                    else
                    {
                        PlayerStatus.Reset();
                    }
                }
                else if (message.GetType() == typeof(StartGame))
                {
                    log.Debug("Stream Message: start game");
                    if (!gameBrainStarted)
                    {
                        PlayerStatus.GameStarted = true;
                    }
                }
                else if (message.GetType() == typeof(KeyInfo))
                {
                    log.Debug("Keyinfo message:  " + (message as KeyInfo).ProcessId);
                    log.Debug("Exponent: " + FormatByteArray((message as KeyInfo).PublicKeyExponent));
                    log.Debug("Modulus: " + FormatByteArray((message as KeyInfo).PublicKeyModulus));
                  
                    PlayerStatus.haveKeys = true;
                    PlayerStatus.PublicKeyExponents.TryAdd((message as KeyInfo).ProcessId, (message as KeyInfo).PublicKeyExponent);
                    PlayerStatus.PublicKeyModuluss.TryAdd((message as KeyInfo).ProcessId, (message as KeyInfo).PublicKeyModulus);  
                }
            }
        }

        private string FormatByteArray(byte[] data)
        {
            string result = "";
            foreach (byte d in data)
                result += string.Format("{0} ", d);
            return result;
        }

        private Boolean sendStream(StreamMessage message)
        {
            bool result = false;
            if (netStream != null && message != null)
            {
                byte[] messageBytes = message.Encode();
                byte[] lengthBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(messageBytes.Length));
                if (netStream.CanWrite)
                {
                    try
                    {
                        netStream.Write(lengthBytes, 0, lengthBytes.Length);
                        netStream.Write(messageBytes, 0, messageBytes.Length);
                        result = true;
                        log.Debug("Stream Message Sent: " + message.GetType().ToString());
                        
                    }
                    catch (Exception err)
                    {
                        log.Debug(err.ToString());
                        System.Console.Write(err);   
                    }
                }

            }
            return result;
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;
            int retries = 0;
            Boolean sentKeys = false;

            exit = false;
            while (!exit) //run until told to stop
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if (retries > PlayerStatus.retries)
                {
                    break;
                }

                /* advance to next state when current is completed */
                if (stageState[stage_index] == State.Complete)
                {
                    stage_index++;

                    /* we are done kill conversation */
                    if (stage_index >= stageState.Length - 1)
                    {
                        exit = true;
                        break;
                    }
                }

                /* if this state hasnt started, start it */
                if (stageState[stage_index] == State.Not_Started)
                {
                    if (stage[stage_index] == typeof(StreamMessage))
                    {
                        stageState[stage_index] = State.In_Process;
                        tcpClient = new TcpClient();
                        tcpClient.Connect(tcpPort.IPEndPoint);
                        netStream = tcpClient.GetStream();
                        netStream.ReadTimeout = 100;
                    }
                }

                /* handle timeout and retries */
                else if (stageState[stage_index] == State.In_Process)
                {
                    if (stage[stage_index] != typeof(StreamMessage))
                    {
                        timeout++;

                        if (timeout > (PlayerStatus.timeout / 10))
                        {
                            //retry
                            retries++;
                            timeout = 0;
                            stage_index = 0;
                            stageState = new State[] { State.Not_Started, State.Not_Started, State.Not_Started };
                        }
                    }
                    else
                    {
                        log.Debug("In stream message");
                        //check pennies used
                        while(PlayerStatus.used_pennies.Count > 0)
                        {
                            Penny outP = null;
                            PlayerStatus.used_pennies.TryDequeue(out outP);

                            if(outP != null)
                            {
                                PennyUsed pennyMessage = new PennyUsed();
                                pennyMessage.SeqNr = 0;
                                pennyMessage.PennyId = outP.Id;
                                sendStream(pennyMessage);
                            }
                        }

                        //check for send keys to gm
                        if(PlayerStatus.MyKeyExponent != null && !sentKeys && PlayerStatus.MyKeyModulus != null)
                        {
                            KeyInfo keyMessage = new KeyInfo();
                            keyMessage.ProcessId = PlayerStatus.myId;
                            keyMessage.PublicKeyExponent = PlayerStatus.MyKeyExponent;
                            keyMessage.PublicKeyModulus = PlayerStatus.MyKeyModulus;
                            keyMessage.SeqNr = 0;
                            sendStream(keyMessage);
                            sentKeys = true;
                        }
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(ReadyToStart))
                    {
                        if (parseReadyToStartReply(check.message as ReadyToStart))
                        {
                            stageState[stage_index] = State.Complete;
                            tcpPort = (check.message as ReadyToStart).StreamEP;
                            gameInfo = (check.message as ReadyToStart).Game;
                            gameId = (check.message as ReadyToStart).GameId;
                        }
                    }
                    if (check.message.GetType() == typeof(EndGame))
                    {
                        if (parseEndGame(check.message as EndGame))
                        {
                            stageState[stage_index] = State.Complete;
                            exit = true;
                            break;
                        }
                    }
                }

                if (stageState[stage_index] == State.In_Process && stage[stage_index] == typeof(StreamMessage))
                {
                    if (netStream.CanRead)
                    {
                        byte[] bytes = new byte[4];
                        int bytesRead = 0;
                        try
                        {
                            while (netStream.CanRead && bytesRead < bytes.Length)
                                bytesRead += netStream.Read(bytes, 0, bytes.Length);
                        }
                        catch (IOException e)
                        {
                            //log.Debug(e.ToString());
                            System.Console.WriteLine(e.ToString());
                        }

                        if (bytesRead == bytes.Length)
                        {
                            int messageLength = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
                            bytes = new byte[messageLength];

                            bytesRead = 0;

                            try
                            {
                                while (netStream.CanRead && bytesRead < bytes.Length)
                                    bytesRead += netStream.Read(bytes, bytesRead, bytes.Length - bytesRead);
                            }
                            catch (IOException e)
                            {
                                //log.Debug(e.ToString());
                                System.Console.WriteLine(e.ToString());
                            }
                        }

                        if (bytesRead == bytes.Length)
                        {
                            StreamMessage message = StreamMessage.Decode(bytes);
                            parseStreamMessage(message);
                        }
                    }
                }
                log.Debug("Game Status Still Running....");
                Thread.Sleep(10);
            }
            log.Debug("Game Status Stopped....");
            if (tcpClient != null && netStream != null)
            {
                tcpClient.Close();
                netStream.Close();
            }
        }
    }
}
