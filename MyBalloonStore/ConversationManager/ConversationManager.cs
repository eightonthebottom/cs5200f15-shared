﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using Controller;
using QueueManagerName;
using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Messages.StatusMessages;
using Players;
using Connection_UDP_JSON;

namespace ConversationManagerName
{
    public class ConversationManager
    {
        public Communicator communicator;
        public Boolean exitSend { get; set; }
        public Boolean exitReceive { get; set; }
        public Boolean exitCreate { get; set; }
        public Player player;

        public ConversationManager(Player p)
        {
            player = p;

            QueueManager.Init();

            MessageNumber.LocalProcessId = 0;
            communicator = new Communicator();

            ThreadPool.SetMaxThreads(10, 10);

            Thread sendThread = new Thread(new ThreadStart(runGetEnvelopePoll));
            sendThread.Start();
            Thread receiveThread = new Thread(new ThreadStart(runSendEnvelopePoll));
            receiveThread.Start();
            Thread newConversation = new Thread(new ThreadStart(runConverstationStarter));
            newConversation.Start();
        }

        public void kill()
        {
            exitSend = true;
            exitReceive = true;
            exitCreate = true;
        }

        private void startLoginProcess()
        {
            Message m = new LoginRequest();
            m.MessageNr = MessageNumber.Create();
            MessageNumber conId = MessageNumber.Create();
            Envelope e = new Envelope(m, conId, EndPointParser.Parse(player.endpoint));

            QueueManager.addEnvelopeToQueue(e);
        }

        private void startJoinGameProcess()
        {
            Message m = new JoinGameRequest();
            m.MessageNr = MessageNumber.Create();
            MessageNumber conId = MessageNumber.Create();
            Envelope e = new Envelope(m, conId, EndPointParser.Parse(player.endpoint));

            QueueManager.addEnvelopeToQueue(e);
        }

        private void runConverstationStarter()
        {
            exitCreate = false;
            while (!exitCreate)
            {
                Envelope envelope = QueueManager.getMessageDefaultQueue();

                if (envelope != null)
                {
                    Conversation c = ConversationFactoryBS.create(envelope.message.GetType(), envelope.converstationID, envelope.message.MessageNr, player, envelope.endpoint);
                    if (c != null)
                    {
                        QueueManager.addNewQueueToDictionary(envelope.converstationID);
                        ThreadPool.QueueUserWorkItem(c.runConversation, null);

                        if (envelope.message.GetType() == typeof(ReadyToStart))
                        {
                            QueueManager.addEnvelopeToQueue(envelope);
                        }
                    }
                }

                /* check player status to init my messages */
                if (!PlayerStatus.LoggedIn && !PlayerStatus.LogInSent)
                {
                    startLoginProcess();
                    PlayerStatus.LogInSent = true;
                }

                if (PlayerStatus.LoggedIn && !PlayerStatus.JoinedGame && !PlayerStatus.JoinGameSent)
                {
                    startJoinGameProcess();
                    PlayerStatus.JoinGameSent = true;
                }

                if (PlayerStatus.GameStarted)
                {
                    if (PlayerStatus.buyBalloon)
                    {
                        PlayerStatus.buyBalloon = false;
                        //startBuyBalloonProcess();
                    }
                    if (PlayerStatus.fillBalloon)
                    {
                        PlayerStatus.fillBalloon = false;
                        //startFillBalloonProcess();
                    }
                    if (PlayerStatus.throwBalloon)
                    {
                        PlayerStatus.throwBalloon = false;
                        //startThrowBalloonProcess();
                    }
                }

                Thread.Sleep(1);
            }
        }

        private void runGetEnvelopePoll()
        {
            exitReceive = false;
            while (!exitReceive)
            {
                Envelope envelope = communicator.getEnvelope();

                if (envelope != null)
                {
                    QueueManager.addEnvelopeToQueue(envelope);
                }

                Thread.Sleep(5);
            }
        }

        private void runSendEnvelopePoll()
        {
            exitSend = false;
            while (!exitSend)
            {
                Envelope envelope = QueueManager.getSendEnvelopeFromQueue();

                if (envelope != null)
                {
                    communicator.sendEnvelope(envelope);
                }

                Thread.Sleep(5);
            }
        }
    }
}
