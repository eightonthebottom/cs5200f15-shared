﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.RequestMessages;
using Players;
using QueueManagerName;
using Controller;

namespace ConversationManagerName
{
    public class LoginConversationBS : Conversation
    {
        public LoginConversationBS(MessageNumber conversationId, MessageNumber messageNumber, Player player, IPEndPoint remoteEP)
        {
            protocol = Protocol.RequestReply;
            finished = false;
            this.conversationId = conversationId;
            this.messageNumber = messageNumber;
            this.player = player;
            this.remoteEP = remoteEP;
        }

        private Message buildLoginRequest()
        {
            LoginRequest message = new LoginRequest();
            message.SetMessageAndConversationNumbers(messageNumber, conversationId);
            message.ProcessType = ProcessInfo.ProcessType.BalloonStore;
            message.ProcessLabel = "Balloon Store";
            //message.Identity = new IdentityInfo();
            //message.Identity = player.idenitity.Clone();

            return message;
        }

        private Boolean parseLoginReply(LoginReply message)
        {
            if (message.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /* Main conversation loop */
        public override void runConversation(object o)
        {
            int timeout = 0;
            int retries = 0;
            Boolean sent = false;

            exit = false;
            while (!exit) //run until told to stop
            {
                if (PlayerStatus.loggedOut)
                {
                    break;
                }

                if (retries > PlayerStatus.retries)
                {
                    break;
                }

                if (!sent)
                {
                    Envelope envelope = new Envelope(buildLoginRequest(), conversationId, remoteEP);
                    QueueManager.addSendEnvelopeToQueue(envelope);
                    sent = true;
                }
                /* handle timeout and retries */
                else
                {
                    timeout++;

                    if (timeout > (PlayerStatus.timeout / 10))
                    {
                        //retry
                        retries++;
                        timeout = 0;
                        sent = false;
                    }
                }

                /* check conversation queue */
                Envelope check = QueueManager.getMessageByConversationId(conversationId);
                if (check != null)
                {
                    if (check.message.GetType() == typeof(LoginReply))
                    {
                        if (parseLoginReply(check.message as LoginReply))
                        {
                            PlayerStatus.LoggedIn = true;
                            PlayerStatus.myId = ((LoginReply)check.message).ProcessInfo.ProcessId;
                            MessageNumber.LocalProcessId = ((LoginReply)check.message).ProcessInfo.ProcessId;
                            PlayerStatus.myEndpoint = ((LoginReply)check.message).ProcessInfo.EndPoint;
                            exit = true;
                            break;
                        }
                        else
                        {
                            /* start over and try again */
                            retries++;
                            sent = false;
                        }
                    }
                }

                Thread.Sleep(10);
            }
        }
    }

}
