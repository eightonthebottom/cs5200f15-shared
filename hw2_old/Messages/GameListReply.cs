﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages
{
    [DataContract]
    public class GameListReply : Reply
    {
        [DataMember]
        public GameInfo[] GameInfo { get; set; }
    }
}
